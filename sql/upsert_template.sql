MERGE
    `cn-ops-spdigital.tests.octopus_ingestion` AS T
USING
    `cn-ops-spdigital.tests.ext_octopus` AS S  -- JSON files in GCS with prefix YYYY-MM-DD-HH* base on DAG logical date
ON
    S.file_name = T.file_name
WHEN NOT MATCHED THEN
    INSERT (
        file_name
        , measure_time
        , building_name
        , meter_name
        , meter_unit
        , div_by
        , measure_value
        , is_valid_measure_value
        , file_content
        , insertion_time
    )
    VALUES (
        file_name
        , measure_time
        , building_name
        , meter_name
        , meter_unit
        , div_by
        , measure_value
        , is_valid_measure_value
        , file_content
        , CURRENT_TIMESTAMP()
    )
;