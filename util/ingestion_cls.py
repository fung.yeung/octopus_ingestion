from datetime import datetime, timedelta
import json
from typing import List
import logging
import sys

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(name)s - %(levelname)s - %(funcName)s() - %(message)s")
handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(formatter)
handler.setLevel(logging.DEBUG)
logger.addHandler(handler)

from google.cloud import bigquery
from google.cloud.bigquery import ExternalConfig
from google.cloud import storage
from concurrent.futures import ThreadPoolExecutor
from concurrent import futures
from google.api_core.exceptions import NotFound


class OctopusIngestor:
    def __init__(self):
        """_summary_

        Args:
            gcs_blob_prefix (str): YYYY-MM-DD-HH passed in from Airflow tempalte {{ds}} in case of backfill,
            to be use to create gcs folder as well
        """
        self.gcs = storage.Client()
        self.bq = bigquery.Client()
        self.SRC_BUCKET = "gbl-ist-ve-meter-ocr-logs_octopus_veolia_com"
        self.DST_BUCKET = "octopus-water-meter-ingestion"
        self.SRC_BUCKET_OBJ: storage.Bucket = self.gcs.bucket(self.SRC_BUCKET)
        # self.DST_BUCKET = "water_meter_testing"
        # self.GCS_BLOB_PREFIX = gcs_blob_prefix  # 2022-04-06-15
        # self.dev_mode = dev_mode

        logger.debug(f"instantiated OcotpusIngestor")

    def _set_instance_attr(
        self, gcs_blob_prefix: str, dev_mode: bool = False, **kwargs
    ):
        self.dev_mode = dev_mode
        self.GCS_BLOB_PREFIX = gcs_blob_prefix  # 2022-04-06-15
        self.GCS_FOLDER_PREFIX = "/".join(
            self.GCS_BLOB_PREFIX.split("-")[:3]
        )  # gcs_blob_prefix minus hours
        logger.debug(
            f"set attribute for Octotpus Ingestor instance, date_prefix is {gcs_blob_prefix} (dev_mode: {dev_mode}"
        )

    def gcs_get_dst_folder_path(self, folder: str) -> str:
        """_summary_
            e.g. gs://octopus-water-meter-ingestion/staging/2022/04/04/<blob_name>
            gs://octopus-water-meter-ingestion/bak/2022/04/04/<blob_name>
        Args:
            dst_folder (str): _description_

        Returns:
            str: _description_
        """
        return self.DST_BUCKET + f"/{folder}/" + self.GCS_FOLDER_PREFIX

    def gcs_get_src_blobs(self, gcs_blob_prefix=None, **kwargs) -> List[str]:
        """return new file between group's and asia buckets (not ingested files)
        Returns:
            _type_: _description_
        """

        if gcs_blob_prefix is None:
            gcs_blob_prefix = self.GCS_BLOB_PREFIX

        src_blobs = self.gcs.list_blobs(
            bucket_or_name=self.SRC_BUCKET, prefix=gcs_blob_prefix
        )

        self.src_blobs = [b.name for b in src_blobs if b.name.endswith(".txt")]

        logger.debug(f"fetched source blobs list from {self.SRC_BUCKET}")
        return self.src_blobs

    def _gcs_txt_to_json(self, blob_name: str) -> dict:
        """Read src bucket txt file, apply transformation, write to dst bucket"""
        output = dict()
        # 2022-03-15-06-41-10-681962|Shanghai_Gas_M3_1_2022 03 14 14h20m58.jpg.txt

        output["file_name"] = blob_name

        uri_remove_prefix_ts = blob_name.split("|")[1]
        fn_parts = uri_remove_prefix_ts.split("_")
        output["building_name"] = fn_parts[0]
        output["meter_name"] = fn_parts[1]
        output["meter_unit"] = fn_parts[2]
        # https://storage.cloud.google.com/water_meter_testing/2022-03-15-06-41-10-681962%7CShanghai_Gas_M3_1_20220314142058.jpg.txt

        # transformation: divide txt file content value by filename "div_by" position value
        output["div_by"] = int(fn_parts[3])

        # convert measure_time to BQ acceptable format
        measure_time_with_fn_suffix = fn_parts[4]
        measure_time = measure_time_with_fn_suffix.split(".")[0]
        measure_time = datetime.strptime(measure_time, "%Y%m%d%H%M%S").isoformat()
        output["measure_time"] = measure_time

        # TXT is the raw file path in dst bucket, JSON is the actual file got ingested
        # output["gcs_txt_path"] = self.GCS_BLOB_PREFIX + "/" + fn
        # output["gcs_json_path"]
        blob = self.SRC_BUCKET_OBJ.get_blob(blob_name)
        output["file_content"] = blob.download_as_text()

        # the ML result can return stupid value, put a flag to highlight it
        if output["file_content"] == ".":
            output["is_valid_measure_value"] = False
            output["measure_value"] = None
        else:
            output["is_valid_measure_value"] = True
            output["measure_value"] = float(output["file_content"]) / output["div_by"]

        logger.debug(f"applied transformation from txt to json for - {blob_name}")
        return output

    def gcs_write_json_to_bucket(self, blobs: List[str]):
        """apply transformation logic with raw TXT blob, turn them into JSON blob. Multithread were used because reading a GCS object is very slow

        Args:
            blobs (List[storage.Blob]): _description_
        """

        with ThreadPoolExecutor(max_workers=8) as executor:
            fut = [executor.submit(self._gcs_txt_to_json, blob) for blob in blobs]

        # upload blob to staging
        for f in futures.as_completed(fut):
            output_dict = f.result()

            bucket = self.gcs.get_bucket(self.DST_BUCKET)
            fn = output_dict["file_name"].replace(".jpg", "").replace(".txt", ".json")
            blob = bucket.blob("staging/" + self.GCS_FOLDER_PREFIX + "/" + fn)
            blob.upload_from_string(
                data=json.dumps(output_dict), content_type="application/json"
            )
            logger.debug(f"uploaded json file to gs://{self.DST_BUCKET}/{blob.name}")

    def gcs_txt_to_json(self, **kwargs):
        """An entry point for Cloud Function, the list of blobs return can then be stored internally in memory instead of using Airflow Xcom"""
        blobs = self.gcs_get_src_blobs()
        if blobs:
            self.gcs_write_json_to_bucket(blobs)

        blobs_num = len(self.src_blobs)
        msg = f"Number of files in GCS with prefix {self.GCS_BLOB_PREFIX} - {len(self.src_blobs)}"
        logger.debug(msg)

        # for consumption in airlfow to decide whether to continue ingestion
        return msg

    def bq_create_external_tbl(self, **kwargs):
        """create external table, the name is fixed, where the external table point to gcs is base on class init variable "gcs_blob_prefix" """
        if self.dev_mode:
            table_name = "cn-ops-spdigital-dev-dev.test_fung.ext_octopus"
        else:
            table_name = "cn-ops-spdigital.tests.ext_octopus"

        try:
            # self.bq.get_table(table=table_name)
            self.bq.delete_table(table=table_name)
            # return logger.debug(f"external table {table_name} already exist")
        except NotFound:
            pass

        schema = [
            bigquery.SchemaField("file_name", "STRING"),
            bigquery.SchemaField("measure_time", "TIMESTAMP"),
            bigquery.SchemaField("building_name", "STRING"),
            bigquery.SchemaField("meter_name", "STRING"),
            bigquery.SchemaField("meter_unit", "STRING"),
            bigquery.SchemaField("div_by", "INT64"),
            bigquery.SchemaField("measure_value", "FLOAT64"),
            bigquery.SchemaField("is_valid_measure_value", "BOOLEAN"),
            bigquery.SchemaField("file_content", "STRING"),
        ]

        table = bigquery.Table(table_name, schema)
        table.expires = datetime.utcnow() + timedelta(days=3)
        # table.clustering_fields = ["file_name"]

        ext_config = ExternalConfig("NEWLINE_DELIMITED_JSON")
        source_folder = self.gcs_get_dst_folder_path(folder="staging")
        source_uris = "gs://" + source_folder + "/" + self.GCS_BLOB_PREFIX + "*.json"
        ext_config.source_uris = source_uris
        table.external_data_configuration = ext_config

        self.bq.create_table(table=table, exists_ok=True)
        logger.debug(f"created external table {table_name}")

    def bq_execute_upsert_query(self, **kwargs):
        """execute static upsert query"""
        with open("sql/upsert_template.sql") as f:
            query = f.read()
        self.bq.query(query=query)
        logger.debug(f"executed upsert statment")

    def gcs_bak_file(self, src_blobs, dest_folder, **kwargs):
        pass


if __name__ == "__main__":
    from rich import print

    gcs_blob_prefix = "2022-03-15-03"
    dev_mode = False
    oi = OctopusIngestor(gcs_blob_prefix=gcs_blob_prefix, dev_mode=dev_mode)
    blobs = oi.gcs_get_src_blobs()
    oi.gcs_write_json_to_bucket(blobs)
    oi.bq_create_external_tbl()
    oi.bq_execute_upsert_query()
