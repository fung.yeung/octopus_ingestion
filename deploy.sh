gcloud functions deploy octopus_ingestion \
    --region us-central1 \
    --runtime python39 \
    --memory 512MB \
    --entry-point main \
    --trigger-http \
    --service-account=octopus-api-caller@cn-ops-spdigital.iam.gserviceaccount.com \
    --project cn-ops-spdigital