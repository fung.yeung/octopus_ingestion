from http import HTTPStatus
import traceback
import logging
import sys

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(name)s - %(levelname)s - %(funcName)s() - %(message)s")
handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(formatter)
handler.setLevel(logging.DEBUG)
logger.addHandler(handler)

from util.ingestion_cls import OctopusIngestor


def main(request=None):
    """This is the first attempt to host Python Class in a Cloud Function, request is expected to have a json body of
        {
            "method_name" (str): <method to be called for the OctopusIngestor class
            "params" (dict): <params that need to be passed to the particular class instance method>
        }

        Example:
            {
                "method_name": "gcs_txt_to_json",
                "params": {
                            "gcs_blob_prefix": "2022-03-15-06",
                            "dev_mode": false
                        }
            }


    Args:
        request (_type_, optional): _description_. Defaults to None.

    Returns:
        _type_: _description_
    """
    if isinstance(request, dict):
        request_json = request
    else:  # on cloud
        request_json = request.get_json(silent=True)

    try:

        params = request_json["params"]
        method_name = request_json["method_name"]
        oi = OctopusIngestor()
        # oi._set_instance_attr(**params)

        func = getattr(oi, method_name)
        logger.debug(f"executing {method_name}")
        output = func(**params)

        if output is None:
            output = f"executed function- {method_name}"
        logger.debug(output)
        # return "yes"
        return output, HTTPStatus.OK

    except Exception:
        logger.debug("There is unknown error")
        err = traceback.format_exc()
        logger.debug(err)
        return err, HTTPStatus.INTERNAL_SERVER_ERROR

    # gcs_blob_prefix = params.get("gcs_blob_prefix")
    # dev_mode = params.get("dev_mode", False)

    # oi = OctopusIngestor(gcs_blob_prefix=gcs_blob_prefix, dev_mode=dev_mode)
    # blobs = oi.gcs_get_src_blobs()
    # oi.gcs_write_json_to_bucket(blobs)
    # oi.bq_execute_upsert_query()

    # return (
    #     f"Ingestion completed for gcs files with prefix - {gcs_blob_prefix}",
    #     HTTPStatus.OK,
    # )


if __name__ == "__main__":
    from rich import print

    ingestion_order = [
        "gcs_txt_to_json",
        "bq_create_external_tbl",
        "bq_execute_upsert_query",
    ]
    for m in ingestion_order:
        d = dict()
        d["method_name"] = m
        d["params"] = {"gcs_blob_prefix": "2022-03-15-06", "dev_mode": False}
        print(d)
        main(request=d)
    # gcs_blob_prefix = "2022-03-15"
    # oi = OctopusIngestor(gcs_blob_prefix=gcs_blob_prefix, dev_mode=False)
    # blobs = oi.gcs_get_src_blobs()
    # oi.gcs_write_json_to_bucket(blobs)
    # oi.bq_execute_upsert_query()
    # src_blobs = oi.gcs_get_diff_file()
    # oi.gcs_txt_to_json(src_blobs)
    # print(src_blobs)
    # for sb in src_blobs:
    # gcs_txt_to_json(sb)
    #     print(sb)
    # ['bucket', 'client', 'pages', 'path', 'prefixes']
    # print(set(src_blobs))
    pass
